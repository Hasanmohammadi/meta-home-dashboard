import React from "react";
import InputCP from "./components/InputCP";
import PeriodInputCP from "./components/PeriodInputCP";
import SelectCP from "./components/SelectCP";

const Dashboard = () => {
  return (
    <div className="text-yellow-400 pr-12 pl-10 mt-10">
      <div className="flex">
        <div className="w-1 h-6 rounded-lg bg-main-green self-center mt-1"></div>
        <h1 className="text-lg font-bold mr-4">خانه هوشمند / ادمین پنل</h1>
      </div>
      <div className="pr-2">
        <div className="mt-11 flex flex-wrap gap-2">
          <InputCP label="نام" />
          <InputCP label="شماره تلفن" />
          <InputCP label="ایمیل" />
          <SelectCP label="شهر" options={[{ value: "1", text: "tehran" }]} />
          <SelectCP
            label="نوع دستگاه"
            options={[{ value: "1", text: "tehran" }]}
          />
          <SelectCP label="خانه" options={[{ value: "1", text: "tehran" }]} />
          <PeriodInputCP label="بازه مصرف" />
          <button className="bg-main-orange px-4  rounded-lg h-16">
            جستجو
          </button>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
