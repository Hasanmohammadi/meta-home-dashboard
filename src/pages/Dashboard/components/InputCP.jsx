const InputCP = ({ label }) => (
  <div className="relative">
    <span className="absolute -top-4 bg-white right-4 px-2 font-bold text-lg text-main-green">
      {label}
    </span>
    <input className="w-[123px] border border-main-green rounded-lg h-16" />
  </div>
);

export default InputCP;
