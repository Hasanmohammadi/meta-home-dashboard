import clsx from "clsx";
import { useState } from "react";

const PeriodInputCP = ({ label }) => {
  const [openPeriodModal, setOpenPeriodModal] = useState(false);
  return (
    <>
      <div
        className={clsx("absolute top-0 right-0 w-screen h-screen", {
          hidden: !openPeriodModal,
          block: openPeriodModal,
        })}
        onClick={() => setOpenPeriodModal(false)}
      ></div>
      <div className="relative">
        <span className="absolute -top-4 bg-white right-4 px-2 font-bold text-lg text-main-green">
          {label}
        </span>
        <input
          className="w-[123px] border border-main-green rounded-lg h-16"
          onClick={() => setOpenPeriodModal((pre) => !pre)}
        />
        <div
          className={clsx(
            "h-52 p-6 shadow-lg border absolute w-96 -right-36 top-[70px]",
            {
              hidden: !openPeriodModal,
              block: openPeriodModal,
            }
          )}
        >
          <p className="text-lg font-bold ">بازه مصرف</p>
          <div className="mt-5 text-center">
            از
            <input className="w-16 mx-1 border border-main-orange" />
            kwh , تا
            <input className="w-16 mx-1 border border-main-orange" />
            kwh
            <br />
            <button className="mt-7 bg-main-orange w-11/12 m-auto py-3 rounded-lg font-bold text-lg">
              تایید
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default PeriodInputCP;
