const SelectCP = ({ label, options, name, id }) => (
  <div className="relative">
    <span className="absolute -top-4 bg-white right-4 px-2 font-bold text-lg text-main-green">
      {label}
    </span>
    <select
      name={name}
      id={id}
      className="w-[123px] border border-main-green rounded-lg h-16"
    >
      {options.map((option) => (
        <option className="px-4" value={option.value}>
          {option.text}
        </option>
      ))}
    </select>
  </div>
);

export default SelectCP;
