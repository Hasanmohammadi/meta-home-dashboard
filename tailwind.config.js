/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,jsx}"],
  theme: {
    extend: {},
    colors: {
      'main-green': "#487B78",
      white: "#FFFFFF",
      'main-orange': "#D36C4C"
    }
  },
  plugins: [],
}
